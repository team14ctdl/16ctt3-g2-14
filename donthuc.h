﻿#pragma once
#include <iostream>
#include <fsteam>
using namespace std;
class donthuc {
private:
	float heso;
	char *bienso;
	int *somu;
	int size;
public:
	donthuc();
	donthuc(float, char*, int*);// nhập vào đơn thức
	~donthuc() {};
	bool operator!(); //rút gọn đơn thức
	bool operator>(const donthuc&); //so sanh don thuc lon hon don thuc
	bool operator+=(const donthuc&);//cong 2 don thuc
	bool operator-=(const donthuc&);//tru 2 don thuc
	bool operator*=(const donthuc&); // nhan 2 don thuc
	friend ostream& operator<<(ostream&, const donthuc&);
}