class dathuc{
private:
	donthuc *arr;
	short size;
public:
	dathuc();
	dathuc(char*);
	~dathuc();
	bool operator!(); //rút gon da thuc
	bool chuanhoa(); //chuan hoa da thuc
	dathuc operator+(const dathuc&); //cong da thuc
	dathuc operator-(const dathuc&); //tru da thuc
	dathuc operator*(const dathuc&); //nhan da thuc
	friend ostream& operator<<(ostream&,const dathuc&);
}

//Giải thích lí do chọn lớp mảng đơn thức cho đa thức: 
//- khi nhập dữ liệu vào thì sẽ được nhập vào mảng động kí tự trước sau đó mới chuyển qua lớp đa thức
//- dễ cho việc xử lí rút gọn, chuẩn hóa, cộng, trừ, nhân, xuất đa thức